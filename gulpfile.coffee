gulp = require 'gulp'
del  = require 'del'


# hot load node modules prefixed with 'gulp-' and put that under the 'plugins' namespace, sans the prefix
# eg. gulp-less becomes plugins.less()
plugins = require('gulp-load-plugins')()

### Configuration ###
## remove any hard coded paths and set them as properties of a JSON file ##
config = require './gulpconfig/gulpconfig.json'

## Env settings ##
isProd = ->
  require('yargs').argv.production


### Batches & Aliases ###

gulp.task 'default', ['clean', 'less', 'browserify']


### Tasks: the meat! ###

# remove generated files
gulp.task 'clean', ->
  del [
    config.less.dest,
    config.js.dest
  ]


# generate our CSS from Less CSS
gulp.task 'less', ->

  lessOptions =
    'paths': config.less.bootstrap
    'compress': isProd()
    'sourceMap': true
    'clean-css': true

  gulp.src config.less.src
    .pipe plugins.plumber()
    .pipe plugins.sourcemaps.init loadMaps: true
    .pipe plugins.less lessOptions
    # externalize the sourceMap (so our users don't pay for it... in KB)
    .pipe plugins.sourcemaps.write config.less.sourcemap
    .pipe gulp.dest config.less.dest


gulp.task 'browserify', ->

  gulp.src config.browserify.options.main, read: false
    .pipe plugins.plumber()
    .pipe plugins.browserify( config.browserify.options )
    .pipe plugins.rename config.js.outputTarget
    .pipe gulp.dest( config.js.dest )


gulp.task 'watch', ->

  # FUTURE: Live Reload
  # server = livereload()

  gulp.watch config.less.watch, ['less']
  gulp.watch config.js.src, ['lint']

  rebundle = ->

    bundler.bundle()
      .pipe source config.browserify.options.main
      .pipe plugins.rename config.js.outputTarget
      .pipe gulp.dest config.js.dest

  bundler = watchify config.browserify.options.main

  # TODO: add UMD transforms
  config.browserify.options.transform.map( (process) ->
    console.log 'transforming via ', process
    bundler.transform process
  )

  bundler.on "update", rebundle
  rebundle()
