# Backbone + Gulp + Browserify + Karma = WIN!

I find this stack to be very useful, and I use it on several of my projects.  After playing with Yeoman, and several other scaffolding frameworks, I found most of them don't fit my needs, are too opinionated, or take too long to set up, and when something goes wrong... I spend the time I would have spent setting this up unraveling code. 

The purpose of this repo is to create a simple boilerplate that I (or anyone) care reuse to kickstart small Backbone projects.

## Getting Started

    npm install
    gulp build

## more doc soon
